## Application-Tasks-Debian

This repo contains application task's solutions for Android SDK Tools in Debian Project.

***Task 1*** -  Find out what new/missing components (D8/R8? hidl-gen? sdkmanager?) and the location of their source code.

***Solution*** - I compared ***hidl-gen*** module from Android Tools repo of Debian [here](https://salsa.debian.org/android-tools-team/android-platform-system-tools-hidl) and AOSP project [here](https://android.googlesource.com/platform/system/tools/hidl/+/refs/heads/master) and found some new and deleted files from the upstream project. They are listed below - 

##### New Files 
* [generateDependencies.cpp](https://android.googlesource.com/platform/system/tools/hidl/+/refs/heads/master/generateDependencies.cpp)
* [generateFormattedHidl.cpp](https://android.googlesource.com/platform/system/tools/hidl/+/refs/heads/master/generateFormattedHidl.cpp)
* [generateJava.cpp](https://android.googlesource.com/platform/system/tools/hidl/+/refs/heads/master/generateJava.cpp)
* [generateJavaImpl.cpp](https://android.googlesource.com/platform/system/tools/hidl/+/refs/heads/master/generateJavaImpl.cpp)

##### Deleted Files 
* RefType.cpp
* RefType.h

***Task 2*** - Test Android Tools bash completion (e.g for adb, fastboot etc), file bugs if it doesn't work properly.

***Solution*** -  Searched for scripts for adb and fastboot, found it [here](https://salsa.debian.org/android-tools-team/android-tools/-/tree/master/debian%2Fbash_completion.d) and tested commands for adb.
* ![](Images/devices.png) <hr>

* ![](Images/version.png) <hr>

* ![](Images/Install_Uninstall.png) <hr>

* ***Will try this in an emulator*** <br>
  ![](Images/adb_root.png) <hr>

* ![](Images/Reboot.png) <hr>

* ![](Images/help.png) <hr>

Commands as push and pull needed root access which I will try in the emulator and explore fastboot as well. Will report bugs if I encounter one.

***Task 3*** - This task is to do anything that I feel suitable.

***Solution*** - After going through resources and some of the DebConf videos to get started with packaging, I seleted the [signapk tool](https://android.googlesource.com/platform/build/+/master/tools/signapk).
Although it was already packaged for Debian, I just wanted to get started. After packaging, I have uploaded the package here on GitLab - [link](https://gitlab.com/dipu989/signapk-tool)

Will add further progress as I continue to learn more.

### Edit (24/04/2020)

I have started with Java packages updation and have updated a few of the packages with latest upstream sources.
My recent activities can be found here - https://salsa.debian.org/users/shantnu-guest/activity

I have learned to use several tools used for packaging like gbp, debuild, uscan, dpkg and a few combinations of gbp and pbuilder and used lintian alognside to resolve warnings.
Honestly, apart from help with mentors and fellow developers in the irc channels, I have surely learnt from the work of previous interns and current contributors.
Things that were not making a lot of sense previously are making sense now. Still I have a long way to go.

I have setup Kotlin and I will update a few android tools to get going.

Thanks for all the efforts in helping me to learn new stuffs!


